package network

import (
	"net"
)

type ServerSocket struct {
	server  *net.UDPAddr
	gateway *net.UDPAddr
	conn    *net.UDPConn
}

func NewServerSocket(gateway string, server string) (*ServerSocket, error) {
	srv, err := net.ResolveUDPAddr("udp", server)
	if err != nil {
		return nil, err
	}
	gtw, err := net.ResolveUDPAddr("udp", gateway)
	if err != nil {
		return nil, err
	}
	sock := &ServerSocket{
		server:  srv,
		gateway: gtw,
	}
	sock.conn, err = net.DialUDP("udp", sock.gateway, sock.server)
	if err != nil {
		return nil, err
	}
	return sock, nil
}

func (sock *ServerSocket) SendToServer(data []byte) error {
	_, err := sock.conn.Write(data)
	return err
}
