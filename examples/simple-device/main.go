package main

import (
	"fmt"
	"os"
	"time"

	"nanomsg.org/go/mangos/v2"
	"nanomsg.org/go/mangos/v2/protocol/pair"

	"gitlab.com/emulora/emulator/messages/payload"

	// register transports
	_ "nanomsg.org/go/mangos/v2/transport/all"
)

func die(format string, v ...interface{}) {
	fmt.Fprintln(os.Stderr, fmt.Sprintf(format, v...))
	os.Exit(1)
}

func main() {
	var sock mangos.Socket
	var err error
	url := "tcp://localhost:95"
	if sock, err = pair.NewSocket(); err != nil {
		die("can't get new pair socket: %s", err.Error())
	}
	if err = sock.Dial(url); err != nil {
		die("can't dial on pair socket: %s", err.Error())
	}

	for {
		payload.N
		if err := sock.Send(); err != nil {
			die("failed sending: %s", err)
		}
		time.Sleep(10 * time.Second)
	}

}
