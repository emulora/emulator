package pull

import (
	"bytes"

	"gitlab.com/emulora/emulator/messages"
)

type Data struct {
	Version   [1]byte           // always 2
	Token     [2]byte           //
	PacketID  messages.PacketID // always 2 for PULL_DATA
	GatewayID commons.GatewayID // identifies gateway
}

func NewDataPull(gatewayID commons.GatewayID) *Data {
	return &Data{
		Version:   [1]byte{0x02},
		Token:     messages.RandomToken(),
		PacketID:  0x02,
		GatewayID: gatewayID,
	}
}

func (data *Data) toBytes() []byte {
	buffer := bytes.NewBuffer(data.Version[:])
	buffer.Write(data.Token[:])
	data.PacketID.Write(buffer)
	buffer.Write(data.GatewayID[:])
	return buffer.Bytes()
}
