package pull

import (
	"bytes"
	"encoding/json"

	"gitlab.com/emulora/emulator/messages"
)

type Resp struct {
	Version     [1]byte           // should be always 2
	Token       [2]byte           // random token
	PacketID    messages.PacketID // should be always 3 for PULL_RESP
	JSONPayload []byte            // valuable info
}

func NewResp(frameData []byte) *Resp {
	return &Resp{
		Version:     [1]byte{frameData[0]},
		Token:       [2]byte{frameData[1], frameData[2]},
		PacketID:    messages.PullRespID,
		JSONPayload: frameData[4:],
	}
}

type TXAck struct {
	Version     [1]byte           // should be always 2
	Token       [2]byte           // same token as in Resp
	PacketID    messages.PacketID // should be always 5 for TX_ACK
	GatewayID   commons.GatewayID // identifies gateway
	JSONPayload []byte            // optional error
}

func (data *TXAck) toBytes() []byte {
	buffer := bytes.NewBuffer(data.Version[:])
	buffer.Write(data.Token[:])
	data.PacketID.Write(buffer)
	buffer.Write(data.GatewayID[:])
	buffer.Write(data.JSONPayload)
	return buffer.Bytes()
}

func NewTXAck(gatewayID commons.GatewayID, token [2]byte, payload interface{}) (*TXAck, error) {
	data, err := json.Marshal(payload)
	if err != nil {
		return nil, err
	}
	return &TXAck{
		Version:     [1]byte{0x02},
		Token:       token,
		PacketID:    messages.TXAckID,
		GatewayID:   gatewayID,
		JSONPayload: data,
	}, nil
}

func Acknowledge(gatewayID gateway.ID, resp *Resp, payload interface{}) (*TXAck, error) {
	return NewTXAck(gatewayID, resp.Token, payload)
}
