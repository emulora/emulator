package messages

import "bytes"

type PacketID byte

const (
	PushDataID PacketID = 0x00
	PushAckID  PacketID = 0x01
	PullDataID PacketID = 0x02
	PullRespID PacketID = 0x03
	PullAckID  PacketID = 0x04
	TXAckID    PacketID = 0x05
)

func (packetId *PacketID) Write(buf *bytes.Buffer) {
	buf.Write([]byte{byte(*packetId)})
}
