package messages

import "math/rand"

func RandomToken() [2]byte {
	slice := make([]byte, 2, 2)
	rand.Read(slice)
	var arr [2]byte
	copy(arr[:], slice)
	return arr
}
