package push

import (
	"bytes"
	"encoding/json"

	"gitlab.com/emulora/emulator/commons"
	"gitlab.com/emulora/emulator/messages"
)

type Data struct {
	Version     [1]byte           // always 2
	Token       [2]byte           // random package id for ack
	PacketID    messages.PacketID // always 0 for PUSH_DATA
	GatewayID   commons.GatewayID // identifies gateway
	JSONPayload []byte            // valuable info
}

func NewDataPush(gatewayID commons.GatewayID, payload interface{}) (*Data, error) {

	data, err := json.Marshal(payload)
	if err != nil {
		return nil, err
	}
	return &Data{
		Version:     [1]byte{0x02},
		Token:       messages.RandomToken(),
		PacketID:    messages.PushDataID,
		GatewayID:   gatewayID,
		JSONPayload: data,
	}, nil
}

func (data *Data) ToBytes() []byte {
	buffer := bytes.NewBuffer(data.Version[:])
	buffer.Write(data.Token[:])
	data.PacketID.Write(buffer)
	buffer.Write(data.GatewayID[:])
	buffer.Write(data.JSONPayload)
	return buffer.Bytes()
}
