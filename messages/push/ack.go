package push

import (
	"gitlab.com/emulora/emulator/messages"
)

type Ack struct {
	Version  [1]byte           // should be always 2
	Token    [2]byte           // same token as in Data
	PacketID messages.PacketID // should be always 1 for PUSH_ACK
}

func NewAck(frameData []byte) *Ack {
	return &Ack{
		Version:  [1]byte{frameData[0]},
		Token:    [2]byte{frameData[1], frameData[2]},
		PacketID: messages.PushAckID,
	}
}
