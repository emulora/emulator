package payload

type DownstreamLoRaPayload struct {
	TXPacket LoRaRXPacket
}

type LoRaTXPacket struct {
	DataSizeBytes uint   `json:"size"`
	Base64Data    string `json:"data"`

	SendImmediately           *bool       `json:"imme"`
	SendInUTCTime             *int        `json:"tmst"`
	SendInGPSTime             *int        `json:"tmms"`
	TXCentralFreqMHz          *float32    `json:"freq"`
	TXConcetratorRFChain      *uint       `json:"rfch"`
	TXOutputPowerdBM          *uint       `json:"powe"`
	ModulationID              *Modulation `json:"modu"`
	LoRaDatarateID            *string     `json:"datr"`
	LoRaECCCodingRateID       *string     `json:"codr"`
	LoRaPolarizationInversion *bool       `json:"ipol"`
	PreambleSize              *uint       `json:"prea"`
	DisablePhysicalCRC        *bool       `json:"ncrc"`
}

type LoRaTXAckPayload struct {
	TXPacketAck LoRaTXPacketAck `json:"txpk_ack"`
}

type LoRaError string

const (
	// None means packet has been programmed for downlink
	None LoRaError = "NONE"
	// TooLate means rejected because it was already too late to program this packet for downlink
	TooLate             LoRaError = "TOO_LATE"
	TooEarly            LoRaError = "TOO_EARLY"
	CollisionPacket     LoRaError = "COLLISION_PACKET"
	CollisionBeacon     LoRaError = "COLLISION_BEACON"
	TXFreqNotSupported  LoRaError = "TX_FREQ"
	TXPowerNotSupported LoRaError = "TX_POWER"
	GPSUnlocked         LoRaError = "GPS_UNLOCKED"
)

type LoRaTXPacketAck struct {
	Error LoRaError `json:"error"`
}
