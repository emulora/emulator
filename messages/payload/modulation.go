package payload

type Modulation string

const (
	LORA Modulation = "LORA"
	// FSK  Modulation = "FSK"
	// TODO support FSK
)
