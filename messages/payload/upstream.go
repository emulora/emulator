package payload

import "time"

type LoRaUpstream struct {
	RXPackets []LoRaRXPacket `json:"rxpk"`
	Stat      *LoRaStatus
}

type LoRaStatus struct {
	GatewaySystemTime               time.Time
	UTCTimeISO8601Expanded          string  `json:"time"`
	GPSLatitudeDegree               float32 `json:"lati"` // North is +
	GPSLongitudeDegree              float32 `json:"long"` // East is +
	GPSAltitudeMeters               int     `json:"alti"`
	RadioPacketsReceived            uint    `json:"rxnb"`
	ValidRadioPacketsReceived       uint    `json:"rxok"`
	RadioPacketsForwarded           uint    `json:"rxfw"`
	AcknowledgedDatagramsPercentage int     `json:"ackr"`
	DownlinkDatagramsReceived       uint    `json:"dwnb"`
	PacketsEmitted                  uint    `json:"txnb"`
}

type LoRaRXPacket struct {
	DataSizeBytes uint   `json:"size"`
	Base64Data    string `json:"data"`

	RXPacketTime                  time.Time
	RXPacketUTCTimeISO8601Compact string     `json:"time"`
	GPSTime                       int64      `json:"tmms"`
	RXFinishedTimestamp           uint32     `json:"tmst"`
	RXCentralFreqMHz              float32    `json:"freq"`
	RXConcentratorIFChannel       uint       `json:"chan"`
	RXConcentratorRFChain         uint       `json:"rfch"`
	LoRaDatarateID                string     `json:"datr"`
	LoRaECCCodingRateID           string     `json:"codr"`
	RSSIdBm                       int        `json:"rssi"`
	LoRaSNRdB                     float32    `json:"lsnr"`
	ModulationID                  Modulation `json:"modu"`
	CRCStatus                     CRCStatus  `json:"stat"`
}

type CRCStatus int

const (
	CRCOk   CRCStatus = 1
	CRCFail CRCStatus = -1
	CRCNone CRCStatus = 0
)
