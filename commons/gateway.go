package commons

import (
	"bytes"
	"encoding/hex"
)

type GatewayID [8]byte

func (id *GatewayID) Hexed() string {
	dst := make([]byte, hex.EncodedLen(len(id)))
	hex.Encode(dst, id[:])
	var buffer bytes.Buffer
	for i, c := range string(dst) {
		buffer.WriteRune(c)
		if (i+1)%2 == 0 {
			buffer.WriteString(" ")
		}
	}
	return buffer.String()
}
