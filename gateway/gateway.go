package gateway

import (
	"encoding/base64"
	"encoding/hex"
	"log"

	"github.com/golang/protobuf/proto"
	"gitlab.com/emulora/emulator/commons"
	"gitlab.com/emulora/emulator/messages/payload"
	"gitlab.com/emulora/emulator/messages/push"
	"gitlab.com/emulora/emulator/network"
)

type VirtualGateway struct {
	server  *network.ServerSocket
	id      commons.GatewayID
	Devices []*VirtualDevice
}

func NewGateway(server *network.ServerSocket) *VirtualGateway {
	return &VirtualGateway{
		server:  server,
		id:      [8]byte{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01},
		Devices: make([]*VirtualDevice, 0, 0),
		//TODO get gateway id and devices from configuration
	}
}

func (gw *VirtualGateway) Start() {
	log.Printf("Gateway is starting...")
	log.Printf("Gateway ID is hex { %s}", gw.id.Hexed())
	log.Printf("Pluggin' devices...")
	for _, dev := range gw.Devices {
		dev.Start()
	}
	log.Printf("%d virtual devices started", len(gw.Devices))
}

func (gw *VirtualGateway) ProcessUplink(dev *VirtualDevice, msg []byte) error {
	uplink := &Uplink{}
	err := proto.Unmarshal(msg, uplink)
	if err != nil {
		return err
	}
	log.Printf("Received payload from device %s : %s",
		dev.name,
		hex.Dump(uplink.Payload.MACPayload.FRMPayload.PlainData),
	)
	encrypted := dev.EncryptedPayload(uplink)
	encoded := base64.StdEncoding.EncodeToString(encrypted)

	pushDataPayload := &payload.LoRaUpstream{
		RXPackets: []payload.LoRaRXPacket{
			payload.LoRaRXPacket{
				DataSizeBytes: uint(len(encrypted)),
				Base64Data:    encoded,
				CRCStatus:     payload.CRCOk,
				ModulationID:  payload.LORA,
				//TODO fill other fields
			},
		},
	}

	pushData, err := push.NewDataPush(gw.id, pushDataPayload)
	if err != nil {
		return err
	}
	gw.server.SendToServer(pushData.ToBytes())
	return nil
}
