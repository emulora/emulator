package gateway

import (
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"log"

	"nanomsg.org/go/mangos/v2"
	"nanomsg.org/go/mangos/v2/protocol/pair"
	// Add transports
	_ "nanomsg.org/go/mangos/v2/transport/all"
)

type VirtualDevice struct {
	sock    mangos.Socket
	plugged bool
	name    string
	gateway *VirtualGateway
}

func NewVirtualDevice(url string, gateway *VirtualGateway) (*VirtualDevice, error) {
	var sock mangos.Socket
	var err error
	if sock, err = pair.NewSocket(); err != nil {
		return nil, err
	}
	if err = sock.Listen(url); err != nil {
		return nil, err
	}
	return &VirtualDevice{
		sock:    sock,
		gateway: gateway,
	}, nil
}

func (dev *VirtualDevice) Start() {
	go dev.receiveLoop()
}

func (dev *VirtualDevice) receiveLoop() {
	var msg []byte
	var err error
	for {
		if msg, err = dev.sock.Recv(); err == nil {
			log.Printf("Received message from virtual device")
			if !dev.plugged {
				dev.plugged = true
			}
			err := dev.gateway.ProcessUplink(dev, msg)
			if err != nil {
				log.Printf("Failed to process uplink message for %s : %v", dev.name, err)
			}
		} else {
			log.Printf("Failed to receive message from virtual device: %v", err)
			//TODO maybe end loop here?
			dev.plugged = false
		}
	}
}

func (dev *VirtualDevice) EncryptedPayload(uplink *Uplink) []byte {
	//TODO encrypt payload and pack everything
	// spec: https://lora-alliance.org/sites/default/files/2018-07/lorawan1.0.3.pdf

	var mTypeInt int32 = int32(uplink.Payload.MHDR.MType)
	bufMType := make([]byte, binary.MaxVarintLen64)
	binary.PutUvarint(bufMType, uint64(mTypeInt))

	log.Printf("MTYPE hex: %s", hex.Dump(bufMType))

	buffer := bytes.NewBuffer(bufMType)

	return buffer.Bytes()
}
