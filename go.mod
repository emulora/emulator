module gitlab.com/emulora/emulator

go 1.12

require (
	github.com/golang/protobuf v1.3.1
	nanomsg.org/go/mangos/v2 v2.0.2
)
