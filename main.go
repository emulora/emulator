package main

import (
	"log"
	"os"

	"os/signal"

	"gitlab.com/emulora/emulator/gateway"
	"gitlab.com/emulora/emulator/network"
)

func main() {
	sock, err := network.NewServerSocket("127.0.0.1:1701", "127.0.0.1:1700")
	if err != nil {
		log.Fatalf("Failed to connect to network: %v", err)
	}
	gw := gateway.NewGateway(sock)
	dev, err := gateway.NewVirtualDevice("tcp://:95", gw)
	if err != nil {
		log.Fatalf("Failed to initialize device: %v", err)
	}
	gw.Devices = append(gw.Devices, dev)
	gw.Start()

	done := make(chan os.Signal)
	signal.Notify(done)
	<-done
}
